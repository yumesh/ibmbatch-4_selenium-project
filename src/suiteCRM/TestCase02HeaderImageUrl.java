package suiteCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase02HeaderImageUrl {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		// open the browser
		driver.get("http://alchemy.hguy.co/crm");
	}

	@Test
	public void urlHeaderImage() {
		// URL of the header image
		String urlLink = driver.findElement(By.tagName("a")).getAttribute("href");
		System.out.println("Header Image URL :   " + urlLink);
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
