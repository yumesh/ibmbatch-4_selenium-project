////How to use this in the code we have to check
//JavascriptExecutor executor = (JavascriptExecutor)driver;executor.executeScript("arguments[0].click();", element);
//
//Test Case : 10.	Counting Dashlets
//Goal: Open the homepage and count the number of the dashlets on the page.
//a.	Open the browser to the Alchemy CRM site and login.
//b.	Count the number of Dashlets on the homepage.
//c.	Print the number and title of each Dashlet into the console.
//d.	Close the browser.

//****************************

package suiteCRM;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase10DashletValidation {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeClass
	public void beforeClass() {
		// Initialization of the FirefoxDriver
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 10);

		// Launch FireFox Browser and redirect to Base Suite CRM URL
		driver.get("http://alchemy.hguy.co/crm");
		// Maximizes the browser window
		driver.manage().window().maximize();
		// Waiting for the browse to open
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test(priority = '0')
	public void homePage() {
		// getting the title of the page
		System.out.println("Title of the page:  " + driver.getTitle());

		// finding and passing the user name and password and clicking on LogIn button
		driver.findElement(By.id("user_name")).sendKeys("admin");
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("bigbutton")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test(priority = '1')
	public void dashletCountValidation() {
		// Finding the number of Hea
		List<WebElement> dashletCount = driver
				.findElements(By.xpath("//li[@class='noBullet' and contains(@id,'dashlet')]"));
		System.out.println("Number of Dashlets in the Home page:  " + dashletCount.size());

	}

	@Test(priority = '1')
	public void dashletPrinting() {
		List<WebElement> dashletHeadingTitle = driver
				.findElements(By.xpath(("//td[@class='dashlet-title']//h3//span[2]")));

		for (WebElement webElement : dashletHeadingTitle) {
			System.out.println(webElement.getText());

		}
	}

	@AfterClass
	public void afterClass() {
		// close the browser
		driver.close();
	}
}
