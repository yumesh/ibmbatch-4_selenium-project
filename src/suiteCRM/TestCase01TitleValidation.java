package suiteCRM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase01TitleValidation {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		// Open the browser
		driver.get("https://alchemy.hguy.co/crm/");

		System.out.println();
	}

	@Test
	public void pageTitle() {
		// title of the page
		Reporter.log("Title of the page :  " + driver.getTitle());
	}

	@Test
	public void titleComparison() {
		// comparing using the assert
		Assert.assertEquals(driver.getTitle(), "SuiteCRM");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
