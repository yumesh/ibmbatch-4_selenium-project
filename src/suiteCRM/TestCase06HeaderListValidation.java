package suiteCRM;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase06HeaderListValidation {
	WebDriver driver;
	WebElement loggedUser;
	String user = "admin";
	String user1;
	String homePageUrl;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		// Launch the Firefox browser and redirect to the Base Suite CRM URL
		driver.get("https://alchemy.hguy.co/crm");

	}

	@Test
	public void findUserName() throws InterruptedException {
//		Finding the Username and password text boxes and sending the credentials
		driver.findElement(By.id("user_name")).sendKeys(user);
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		System.out.println("URL page 0:  " + driver.getCurrentUrl());

//		Finding the Loign Button 
		WebElement loginButton = driver.findElement(By.id("bigbutton"));
		System.out.println("LoginButton is Displayed :  " + loginButton.isDisplayed());
		System.out.println("Login Button is Enabled :  " + loginButton.isEnabled());
		System.out.println("Text on the login button:  " + loginButton.getAttribute("value"));

//      Clicking on the Login Button
		loginButton.click();

//		Finding the admin in the Home Page
		loggedUser = driver.findElement(By.xpath("//span[contains(text(),'admin')]"));

		user1 = loggedUser.getText();
		System.out.println("User Logged in with the credentials : " + user1);

		System.out.println("URL page :  " + driver.getCurrentUrl());
		homePageUrl = driver.getCurrentUrl();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);

		List<WebElement> mainList = driver.findElements(By.xpath("//ul[@class='nav navbar-nav']"));
		System.out.println("No of List : " + mainList.size());
		for (WebElement webElement : mainList) {
			String actual =webElement.getText();
			System.out.println("Main menu List on the top Naviagation : " + actual);
			if (actual.equals("ACTIVITIES"))
			{
			System.out.println("ACTIVITIES exists in the navigation menu");
			break;
			}
		}

	}

	@AfterClass
	public void afterClass() {
		driver.close();

	}

}
