package suiteCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase03CopyRightText {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
//		 open the browser
		driver.get("https://alchemy.hguy.co/crm");
	}

	@Test
	public void copyRightPrint() {
		String copyFooter = driver.findElement(By.id("admin_options")).getText();
		System.out.println("First Footer Copy Text :  " + copyFooter);
		String secondFooter = driver.findElement(By.id("powered_by")).getText();
		System.out.println("Second Footer Copy Text:  " + secondFooter);

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
