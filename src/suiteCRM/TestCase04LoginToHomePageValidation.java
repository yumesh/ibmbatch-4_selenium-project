package suiteCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase04LoginToHomePageValidation {
	WebDriver driver;
	WebElement loggedUser;
	String user = "admin";
	String user1;
	String homePageUrl;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		// Open the browser
		driver.get("https://alchemy.hguy.co/crm");

	}

	@Test
	public void findUserName() {
//		Finding the Username and password text boxes and sending the credentials
		driver.findElement(By.id("user_name")).sendKeys(user);
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		System.out.println("URL page 0:  "+driver.getCurrentUrl());

//		Finding the Loign Button 
		WebElement loginButton = driver.findElement(By.id("bigbutton"));
		System.out.println("LoginButton is Displayed :  " + loginButton.isDisplayed());
		System.out.println("Login Button is Enabled :  " + loginButton.isEnabled());
		System.out.println("Text on the login button:  "+ loginButton.getAttribute("value"));

//      Clicking on the Login Button
		loginButton.click(); 

//		Finding the admin in the Home Page
		loggedUser = driver.findElement(By.xpath("//span[contains(text(),'admin')]"));

		user1 = loggedUser.getText();
		System.out.println("User Logged in with the credentials : " + user1);
		
		System.out.println("URL page :  "+driver.getCurrentUrl());	
		homePageUrl=driver.getCurrentUrl();
	}

	@Test
	public void homePageValidation() {

		Assert.assertEquals(user1, user);
		System.out.println("URL page 2 : "+driver.getCurrentUrl());
		
		Assert.assertEquals(homePageUrl, "https://alchemy.hguy.co/crm/index.php?module=Home&action=index");
		

	}

	@AfterClass
	public void afterClass() {
		driver.close();

	}

}
