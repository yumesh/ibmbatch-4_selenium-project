//Test Case 11.	Create multiple leads by importing a CSV file
//Goal: Open the homepage and count the number of the dashlets on the page.
//a.	Open the browser to the Alchemy CRM site and login.
//b.	Navigate to Sales -> Leads -> Import Leads
//c.	Find the import element on the page and select the file to import. (The format of the import file will be given beforehand)
//d.	Click Next and submit to finish the import.
//e.	Navigate to the View Leads page to see results.
//f.	Close the browser.
//
////*********************************************


package suiteCRM;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase11CsvFilealidation {
	WebDriver driver;
	WebDriverWait wait;
	Actions action;
	ExpectedCondition<Boolean> expectation = new
			
		    ExpectedCondition<Boolean>() {
	        public Boolean apply(WebDriver driver) {
	           return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
	            }
	        };    
	

	@BeforeClass
	public void beforeClass() {
		// Initialization of the FirefoxDriver
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 30);
		action = new Actions(driver);		
		
		     
	// Launch FireFox Browser and redirect to Base Suite CRM URL
		driver.get("http://alchemy.hguy.co/crm");
		// Maximizes the browser window
		driver.manage().window().maximize();
		//Waiting for the browse to open
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test(priority = '0')
	public void homePage() {
		// getting the title of the page
		System.out.println("Title of the page:  " + driver.getTitle());

		// finding and passing the user name and password and clicking on LogIn button
		driver.findElement(By.id("user_name")).sendKeys("admin");
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("bigbutton")).click();
	}

	@Test (priority = '1')
	public void salesList()  {
		// Finding the sales in the navigation bar
		WebElement salesMenu = driver.findElement(By.id("grouptab_0"));
		System.out.println("Navigation Menu Name :  " + salesMenu.getText());

		// Declaring the Aciton class for the mouse over
//		Actions action = new Actions(driver);
		action.moveToElement(salesMenu).click().perform();

		// Finding Sales Menu list items
		List<WebElement> salesList = driver.findElements(By.xpath("//li[2]//span[2]//ul[1]"));

		// Nested for loop to Display the entire Sales Menu list
		for (WebElement sales : salesList) {
			System.out.println("Sales menu list: " + sales.getText());
		}		
	}

	@Test(priority = '2')
	public void leadsListValidation() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		// Finding the Leads WebElement
		WebElement leadValue = driver.findElement(By.id("moduleTab_9_Leads"));
		System.out.println("Text on the leads : " + leadValue.getText());
         
     //Clicking on the Lead web element
		leadValue.click();
		 wait.until(expectation); 
		
	}
	
	@Test (priority = '3')
	public void leadsNewListValidation () throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		WebElement leadsNewValue=driver.findElement(By.xpath("//span[@class='currentTab']//a[@id='moduleTab_Leads']"));
	    System.out.println("New Leads Value name :  "+leadsNewValue.getText());
	    wait.until(expectation);
		 action.moveToElement(leadsNewValue).click().perform();

		List<WebElement> leadsNewList= driver.findElements(By.xpath("//li[@class='current-module-action-links']"));
		  System.out.println("Number of Elements in the List :  "+leadsNewList.size());
		  for (WebElement webElement : leadsNewList) {
			  System.out.println("List of Elements in the Leads:  "+webElement.getText());}
		      
			  //Finding the import leads button
			 driver.findElement(By.xpath("//span[contains(text(),'Import Leads')]")).click();
		    
		      
		   WebElement uploadElement = driver.findElement(By.id("userfile"));
	        // enter the file path onto the file-selection input field
	        uploadElement.sendKeys("C:\\Leads.csv");     
	       driver.findElement(By.xpath("//input[@id='gonext']")).click();	  
		   driver.findElement(By.xpath("//input[@id='gonext']")).click();
		   driver.findElement(By.xpath("//input[@id='gonext']")).click();
	       driver.findElement(By.xpath("//input[@id='importnow']")).click();
		   driver.findElement(By.xpath("//input[@id='finished']")).click();
		   }
	
	@Test (priority='4' )
	public void viewLeadsPageResultValidation () throws InterruptedException {
		Thread.sleep(10000);
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
				  "//div[@class='list-view-rounded-corners']/table/tbody/tr[]/td[]")));
		
		 for (int i=1;i<=10;i++)
		 {
				 WebElement name = driver.findElement(By.xpath(
			  "//div[@class='list-view-rounded-corners']/table/tbody/tr["+i+"]/td[3]"));
			String name1=name.getText();
				 System.out.println("Name"+ name1);
			  if(name1.equals("Charlie Beall")) { System.out.println("Name found"+ name1);
				  break; }
				 
				 
	 		  }
	}	
	
	
	@AfterClass
	public void afterClass() {
		// close the browser
		driver.close();
	}
}
