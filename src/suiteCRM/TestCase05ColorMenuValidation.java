package suiteCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase05ColorMenuValidation {
	WebDriver driver;
	WebDriverWait wait;
	WebElement loggedUser;
	String user = "admin";
	String user1;
	String color;
	String hex;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 10);

		// Open the browser
		driver.get("https://alchemy.hguy.co/crm");

	}

	@Test(priority = '0')
	public void findUserName() {
//		Finding the Username and password text boxes and sending the credentials
		driver.findElement(By.id("user_name")).sendKeys(user);
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		System.out.println("URL page before logged in_1:  " + driver.getCurrentUrl());

//		Finding the Loign Button 
		WebElement loginButton = driver.findElement(By.id("bigbutton"));
		System.out.println("LoginButton is Displayed :  " + loginButton.isDisplayed());
		System.out.println("Login Button is Enabled :  " + loginButton.isEnabled());

//      Clicking on the Login Button
		loginButton.click();

//		Finding the admin in the Home Page
		loggedUser = driver.findElement(By.xpath("//span[contains(text(),'admin')]"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'admin')]")));

		user1 = loggedUser.getText();
		System.out.println("User Logged in with the credentials : " + user1);

		System.out.println("URL page after logged in_1 :  " + driver.getCurrentUrl());

		color = driver.findElement(By.id("grouptab_0")).getCssValue("color");
		System.out.println("Color code: " + color);
		hex = Color.fromString(color).asHex();
		System.out.println("Color of the Navigation:" + hex);
	}

	@Test(priority = '2')
	public void homePageValidation() {

		Assert.assertEquals(user1, user);
		System.out.println("URL page after logged in_2 : " + driver.getCurrentUrl());
	}

	@Test(priority = '3')
	public void colorMenu() {

		System.out.println("The Color code: " + color);
		System.out.println("The Color is: " + hex);

	}

	@AfterClass
	public void afterClass() {
		driver.close();

	}

}
