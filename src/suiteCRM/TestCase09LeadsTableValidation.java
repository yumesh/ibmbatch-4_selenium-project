//Test Case :9.	Traversing tables 2
//Goal: Open the leads page and print the usernames and full names from the table.
//a.	Open the browser to the Alchemy CRM site and login.
//b.	Navigate to the Sales -> Leads.
//c.	Find the table on the page and print the first 10 values in the Name column and the User column of the table to the console.
//d.	Close the browser.
//***********************************


package suiteCRM;

import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase09LeadsTableValidation {
	WebDriver driver;
	FluentWait<WebDriver> wait;

	@BeforeClass
	public void beforeClass() {
		// Initialization of the FirefoxDriver
		driver = new FirefoxDriver();
		

		// Launch FireFox Browser and redirect to Base Suite CRM URL
		driver.get("http://alchemy.hguy.co/crm");
		// Maximizes the browser window
		driver.manage().window().maximize();
		//Waiting for the browse to open
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test(priority = '0')
	public void homePage() {
		// getting the title of the page
		System.out.println("Title of the page:  " + driver.getTitle());

		// finding and passing the user name and password and clicking on LogIn button
		driver.findElement(By.id("user_name")).sendKeys("admin");
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("bigbutton")).click();
	}

	@Test(priority = '1')
	public void salesList() {
		// Finding the sales in the navigation bar
		WebElement salesMenu = driver.findElement(By.id("grouptab_0"));
		System.out.println("Navigation Menu Name :  " + salesMenu.getText());

		// Declaring the Aciton class for the mouse over
		Actions action = new Actions(driver);
		action.moveToElement(salesMenu).click().perform();

		// Finding Sales Menu list items
		List<WebElement> salesList = driver.findElements(By.xpath("//li[2]//span[2]//ul[1]"));

		// Nested for loop to Display the entire Sales Menu list
		for (WebElement sales : salesList) {
			System.out.println("Sales menu list: " + sales.getText());
		}
 	}
	@Test(priority = '2')
	public void leadsListValidation() throws InterruptedException {

		// Finding the Leads WebElement
		WebElement leadValue = driver.findElement(By.id("moduleTab_9_Leads"));
		System.out.println("Text on the leads : " + leadValue.getText());

//      Clicking on the Leads web element
		leadValue.click();

		//		Thread.sleep(10000); 
  
		 ExpectedCondition<Boolean> expectation = new				 
	    ExpectedCondition<Boolean>() {
        public Boolean apply(WebDriver driver) {
           return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
            }
        };
        
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(expectation);




	}
	
	@Test (priority ='3')
	public void nameAndUserNameListValidation() {
//		Finding Names List in the 3rd column
		List<WebElement> nameList=driver.findElements(By.xpath	          
			("//div[@id='bootstrap-container']/div[@id='content']/div[@class='listViewBody']/form[@id='MassUpdate']/div[@class='list-view-rounded-corners']/table[@class='list view table-responsive']/tbody/tr/td[3]"));
//		Finding User Name List in the 8th column	
		List<WebElement> userNameList=driver.findElements(By.xpath	          
				("//div[@id='bootstrap-container']/div[@id='content']/div[@class='listViewBody']/form[@id='MassUpdate']/div[@class='list-view-rounded-corners']/table[@class='list view table-responsive']/tbody/tr/td[8]"));
//		Using for loop for the first 10 rowss
		for (int i=0;i<10;i++) {
//			 Printing the Name and User Names of 10 rows in the table
			System.out.println("Name: " +nameList.get(i).getText()    +"   User Name: " +userNameList.get(i).getText());
		}
		
	}

	@AfterClass
	public void afterClass() {
		// close the browser
		driver.close();
	}
}
