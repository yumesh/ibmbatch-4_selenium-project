//TestCase 12.	Schedule a meeting and invite members
//Goal: To schedule a meeting and include at least 3 invitees.
//a.	Open the browser to the Alchemy CRM site and login.
//b.	Navigate to Activities -> Meetings -> Schedule a Meeting.
//c.	Enter the details of the meeting.
//d.	Search for members and add them to the meeting.
//e.	Click Save.
//f.	Navigate to View Meetings page and confirm creation of the meeting.
//g.	Close the browser.

//*****************************

package suiteCRM;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase12ScheduleMeeting {
	WebDriver driver;
	WebDriverWait wait;
	String activitiesMenu;
	String meetingValue="Testing Batch 4";
    ExpectedCondition<Boolean> expectation = new
			
		    ExpectedCondition<Boolean>() {
	        public Boolean apply(WebDriver driver) {
	           return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
	            }
	        };    
	

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver,30);
		// Open the browser
		driver.get("http://alchemy.hguy.co/crm");
	}

	@Test(priority = '0')
	public void findUserName() {

		driver.findElement(By.id("user_name")).sendKeys("admin");
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");

//		Finding the Login Button  
		driver.findElement(By.id("bigbutton")).click();

	}

	@Test(priority = '1')
	public void menuListMouseOver() {
// Find the activities list in the Home Page
		WebElement activitesValue = driver.findElement(By.id("grouptab_3"));

		System.out.println("Name of the Navigation Item:  " + activitesValue.getText());
		// Name of the Activities value passing into the string
		activitiesMenu = activitesValue.getText();

		// Defining the action class for the mouse over activity
		Actions action = new Actions(driver);
		action.moveToElement(activitesValue).click().perform();
		List<WebElement> activitiesList = driver.findElements(By.xpath("//li[5]//span[2]//ul[1]"));

		// ul[@class='dropdown-menu']

		for (WebElement webElement : activitiesList) {

			System.out.println("List of the menu activities: " + webElement.getText());

		}

	}

	@Test(priority = '2')
	public void scheduelMeeting() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		Thread.sleep(5000);
       driver.findElement(By.id("moduleTab_9_Meetings")).click();     
   		
	 driver.findElement(By.xpath("//div[contains(text(),'Schedule Meeting')]")).click();
	 Thread.sleep(5000);
	 //wait.until(expectation); 
	 //wait.until(ExpectedConditions.elementToBeSelected(By.xpath("//input[@id='name']")));
	
	 
	 driver.findElement(By.xpath("//input[@id='name']")).sendKeys("Testing Batch 4");
	 driver.findElement(By.xpath("//input[@id='date_start_date']")).sendKeys("04/06/2020");
	 driver.findElement(By.xpath("//input[@id='date_end_date']")).sendKeys("04/06/2020");
	 driver.findElement(By.xpath("//textarea[@id='description']")).sendKeys("This meeting is for testing purpose");;
     driver.findElement(By.xpath("//input[@id='search_first_name']")).sendKeys("test");
     driver.findElement(By.xpath("//input[@id='invitees_search']")).click();
     driver.findElement(By.xpath("//input[@id='invitees_add_1']")).click();
     driver.findElement(By.xpath("//input[@id='invitees_add_2']")).click();
     driver.findElement(By.xpath("//input[@id='invitees_add_3']")).click();
     //wait.until(expectation); 
     Thread.sleep(5000);
     driver.findElement(By.xpath("//body/div[@id='bootstrap-container']/div[@id='content']/form[@id='EditView']/div[@class='buttons']/input[1]")).click();
	
	}
	
	@Test (priority ='3')
	public void viewMeetingsValidatoin() throws InterruptedException {		
		driver.findElement(By.xpath("//div[contains(text(),'View Meetings')]")).click();
		Thread.sleep(5000);
		for (int i=1;i<5;i++) {
		WebElement subjectValue=driver.findElement(By.xpath
		("//table[@class='list view table-responsive']//tbody//tr[" + i + "]//td[4]"));				
		if (subjectValue.equals(meetingValue)) {
			System.out.println("Meeting scheduled found");
		}
		}
		
	
	
	
	}

	@AfterClass
	public void afterClass() {
		//driver.close();

	}

}
