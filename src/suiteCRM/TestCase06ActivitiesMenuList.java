package suiteCRM;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestCase06ActivitiesMenuList {
	WebDriver driver;
	String activitiesMenu;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		// Open the browser
		driver.get("http://alchemy.hguy.co/crm");

	}

	@Test(priority = '0')
	public void findUserName() {

		driver.findElement(By.id("user_name")).sendKeys("admin");
		driver.findElement(By.id("username_password")).sendKeys("pa$$w0rd");

//		Finding the Login Button  
		driver.findElement(By.id("bigbutton")).click();

	}

	@Test(priority = '1')
	public void menuListMouseOver() {
// Find the activities list in the Home Page
		WebElement activitesValue = driver.findElement(By.id("grouptab_3"));

		System.out.println("Name of the Navigation Item:  " + activitesValue.getText());
		// Name of the Activities value passing into the string
		activitiesMenu = activitesValue.getText();

		// Defining the action class for the mouse over activity
		Actions action = new Actions(driver);
		action.moveToElement(activitesValue).click().perform();
		List<WebElement> activitiesList = driver.findElements(By.xpath("//li[5]//span[2]//ul[1]"));

		// ul[@class='dropdown-menu']

		for (WebElement webElement : activitiesList) {

			System.out.println("List of the menu activities: " + webElement.getText());

		}

	}

	@Test(priority = '2')
	public void menuValidation() {
		Assert.assertEquals(activitiesMenu, "ACTIVITIES");
	}

	@AfterClass
	public void afterClass() {
		driver.close();

	}

}
